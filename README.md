# i.MX Dev Blog

https://imxdev.gitlab.io/

This is a blog to gather information and tutorials regarding i.MX Family Software.

## How to collaborate

You can collaborate discussing on our [mailing list](imxdevblog@googlegroup.com)

You can send a pull request adding a new guest written post, or add yourself as
an author (see _config.yml)

## How to preview the blog on your computer

```
$ apt-get install -y ruby-bundler ruby-dev zlib1g-dev
$ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
$ curl -sSL https://get.rvm.io | bash -s stable
$ source ~/.rvm/scripts/rvm
$ cd blog
$ bundle install
$ bundle exec jekyll build
$ bundle exec jekyll serve
```

Pay attention to the log of last command. It shows the URL for the local server you should open on your browser in order to preview the blog.

You can also use the script ( see `scripts/new_post.sh`) to help you create a blog post template.

## License for Minimal Mistakes theme

The MIT License (MIT)

Copyright (c) 2016 Michael Rose

[in detail here](https://github.com/mmistakes/minimal-mistakes/blob/master/LICENSE)

## License for i.MXDev Blog posts

The content inside **_post** folder is under Creative Commons 4.0 ShareAlike. You can see the terms [here](https://creativecommons.org/licenses/by-sa/4.0/)
