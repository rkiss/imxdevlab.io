---
title: "List of Application notes for i.MX SoC"
excerpt: "A page to list the links to the online Application Notes for the IMX chips"
---

This page points to the PDF files hosted at [NXP site](http://nxp.com). It means
the blog does not keep a copy of the PDF files.

The table below shows the title with a link to the file and its last
modification date.

We still don't have implemented proper ways to parse the title from the pdf file
instead of using the pdf metadata. We are also working to parse older App Notes.

The modification date for the file is not exactly its **revision**, which is
related to the publication date. However the modification date is easily
extracted from PDF metadata.

{% include imxdoc/an-imx-table.md %}

