---
title: "i.MX7 SoC Reference Manuals"
excerpt: "A page to list the links to the online Reference Manual for the imx7 chips"
---
{% include imxdoc/rm-introduction.md %}
{% include imxdoc/rm-imx7-table.md %}
