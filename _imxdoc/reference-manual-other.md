---
title: "SoCs Reference Manual"
excerpt: "A page to list the links to the online Reference Manual for the imx chips"
---
{% include imxdoc/rm-introduction.md %}
{% include imxdoc/rm-imxother-table.md %}
