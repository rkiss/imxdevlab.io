---
title: "i.MX Software Documentation link page"
excerpt: "A page to list all the links to the online i.MX BSP Software documentation."
---

This page points to the PDF files hosted at [NXP site](http://nxp.com).
It means the blog does not keep a copy of the PDF files.

The documents pointed here are for the **latest** BSP release. It means when the
next GA release is out, the link will point to the future release document.

Currently, the latest BSP releases are:

* Linux: **L4.9.88_2.0.0 GA**
* Android: **O8.0.0_1.0.0 GA**

Another good central place to be up-to-date with i.MX software releases is
[i.MX Software and Development Tool](http://nxp.com/imx6tools).

## Linux Documents

[i.MX Linux User's Guide](https://www.nxp.com/docs/en/user-guide/i.MX_Linux_User's_Guide.pdf)

[i.MX Linux Reference Manual](https://www.nxp.com/docs/en/reference-manual/i.MX_Reference_Manual_Linux.pdf)

[i.MX Linux Release Notes](https://www.nxp.com/docs/en/release-note/i.MX_Linux_Release_Notes.pdf)

[i.MX Linux BSP Porting Guide](https://www.nxp.com/docs/en/user-guide/i.MX_BSP_Porting_Guide_Linux.pdf)

[i.MX Linux Graphics User's Guide](https://www.nxp.com/docs/en/user-guide/i.MX_Graphics_User's_Guide_Linux.pdf)

[i.MX Yocto Project's User's Guide](https://www.nxp.com/docs/en/user-guide/i.MX_Yocto_Project_User's_Guide_Linux.pdf)


## Android Documents

[Android User's Guide](https://www.nxp.com/docs/en/user-guide/Android_User's_Guide.pdf)

[Android Release Notes](https://www.nxp.com/docs/en/release-note/Android_Release_Notes.pdf)

[Android Quick Start Guide](https://www.nxp.com/docs/en/user-guide/Android_Quick_Start_Guide.pdf)

[i.MX Android BSP Porting Guide](https://www.nxp.com/docs/en/user-guide/i.MX_BSP_Porting_Guide_Android.pdf)

[i.MX Android Graphic User's Guide](https://www.nxp.com/docs/en/user-guide/i.MX_Graphics_User's_Guide_Android.pdf)

[Android Frequently Asked Questions](https://www.nxp.com/docs/en/reference-manual/Android_Frequently_Asked_Questions.pdf)
