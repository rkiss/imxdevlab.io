This report was generated on Sat Jun 09 14:29:23 UTC 2018

|**Title**|**Last Modification**|
|[Building a Simple 3D Media Player for the i.MX31 in Linux](https://www.nxp.com/docs/en/application-note/AN3997.pdf) |Mar 2010|
|[i.MX35 Memories](https://www.nxp.com/docs/en/application-note/AN3998.pdf) |Mar 2010|
|[i.MX27 to i.MX25 Porting Guide](https://www.nxp.com/docs/en/application-note/AN3999.pdf) |Mar 2010|
|[Interfacing and Configuring the i.MX25 Flash Devices](https://www.nxp.com/docs/en/application-note/AN4016.pdf) |Mar 2010|
|[Interfacing mDDR and DDR2 Memories with i.MX25](https://www.nxp.com/docs/en/application-note/AN4017.pdf) |Mar 2010|
|[Hardware Basic Recommendations for Prototyping with the i.MX31](https://www.nxp.com/docs/en/application-note/AN4040.pdf) |Jan 2010|
|[Different Display Configurations on the i.MX31 WinCE PDK](https://www.nxp.com/docs/en/application-note/AN4041.pdf) |Mar 2010|
|[Image Processing API for the i.MX Platform](https://www.nxp.com/docs/en/application-note/AN4042.pdf) |May 2010|
|[Getting Started with WinCE 6.0 VPU Applications](https://www.nxp.com/docs/en/application-note/AN4043.pdf) |Mar 2010|
|[3D Animation Techniques on the i.MX31 PDK](https://www.nxp.com/docs/en/application-note/AN4044.pdf) |Jan 2010|
|[Building a 3D Graphic User Interface in Linux](https://www.nxp.com/docs/en/application-note/AN4045.pdf) |Jan 2010|
|[Extracting Latitude and Longitude from a GPS Device for Windows Embedded CE 6.0](https://www.nxp.com/docs/en/application-note/AN4046.pdf) |Jan 2010|
|[i.MX233 CPU and HCLK Power Saving Features](https://www.nxp.com/docs/en/application-note/AN4050.pdf) |Jan 2010|
|[i.MX51 EVK Supply Current Measurements](https://www.nxp.com/docs/en/application-note/AN4051.pdf) |May 2010|
|[i.MX51 Power-Up Sequence](https://www.nxp.com/docs/en/application-note/AN4053.pdf) |Oct 2010|
|[i.MX31 PDK Power Measurement with GUI](https://www.nxp.com/docs/en/application-note/AN4061.pdf) |Feb 2010|
|[Changing the i.MX25 NAND Flash Model for Windows Embedded CE 6.0](https://www.nxp.com/docs/en/application-note/AN4111.pdf) |May 2010|
|[Changing the i.MX31 USB PHY](https://www.nxp.com/docs/en/application-note/AN4130.pdf) |May 2012|
|[Getting Started with OpenGL ES, PVR Libraries, and Tools for the i.MX31 PDK](https://www.nxp.com/docs/en/application-note/AN4131.pdf) |May 2010|
|[3D Math Overview and 3D Graphics Foundations](https://www.nxp.com/docs/en/application-note/AN4132.pdf) |May 2010|
|[How to Run BSP Unit Test on Windows CE Using i.MX Platforms](https://www.nxp.com/docs/en/application-note/AN4133.pdf) |May 2010|
|[Hardware Configurations for the i.MX Family USB Modules](https://www.nxp.com/docs/en/application-note/AN4136.pdf) |Jun 2010|
|[BINFS Implementation Guide](https://www.nxp.com/docs/en/application-note/AN4137.pdf) |Jun 2010|
|[Program NAND with ROM Programmer Implementation Guide](https://www.nxp.com/docs/en/application-note/AN4138.pdf) |Jun 2010|
|[Multi-NAND Disks Implementation Guide](https://www.nxp.com/docs/en/application-note/AN4139.pdf) |Jun 2010|
|[i.MX51 WinCE Clock Setting](https://www.nxp.com/docs/en/application-note/AN4140.pdf) |Jun 2010|
|[U-Boot for i.MX25 Based Designs](https://www.nxp.com/docs/en/application-note/AN4171.pdf) |Jul 2010|
|[U-Boot for i.MX35 based Designs](https://www.nxp.com/docs/en/application-note/AN4172.pdf) |Jul 2010|
|[U-Boot for i.MX51 Based Designs](https://www.nxp.com/docs/en/application-note/AN4173.pdf) |Jul 2010|
|[Media Streaming to i.MX31 PDK through Wireless LAN](https://www.nxp.com/docs/en/application-note/AN4174.pdf) |Jul 2010|
|[System-80 Asynchronous Display on the i.MX31 WINCE 6.0 PDK](https://www.nxp.com/docs/en/application-note/AN4180.pdf) |Aug 2010|
|[Different Display Configurations on the i.MX31 Linux PDK](https://www.nxp.com/docs/en/application-note/AN4182.pdf) |Aug 2010|
|[Architectural Differences between the i.MX23, i.MX25, and i.MX28](https://www.nxp.com/docs/en/application-note/AN4198.pdf) |Sep 2010|
|[Using the i.MX28 Power Management Unit and Battery Charger](https://www.nxp.com/docs/en/application-note/AN4199.pdf) |Apr 2015|
|[Modifying Bootloader and Kernel to Support a Different SDRAM on the i.MX25 using WinCE 6.0\xe2\x84\xa2i.MX25 using WinCE 6.0](https://www.nxp.com/docs/en/application-note/AN4200.pdf) |Sep 2010|
|[Modifying Bootloader and Kernel to Support a Different SDRAM on the i.MX51 Using WinCE 6.0\xe2\x84\xa2](https://www.nxp.com/docs/en/application-note/AN4202.pdf) |Feb 2011|
|[imx28_layout_guidelines_rev_0.fm](https://www.nxp.com/docs/en/application-note/AN4215.pdf) |Sep 2010|
|[AN4227, MC13892 Charger Operation](https://www.nxp.com/docs/en/application-note/AN4227.pdf) |Aug 2011|
|[AN4270_Rev1.fm](https://www.nxp.com/docs/en/application-note/AN4270.pdf) |Feb 2012|
|[AN4271.fm](https://www.nxp.com/docs/en/application-note/AN4271.pdf) |Feb 2011|
|[test.fm](https://www.nxp.com/docs/en/application-note/AN4274.pdf) |Feb 2011|
|[AN4380.fm](https://www.nxp.com/docs/en/application-note/AN4380.pdf) |Oct 2011|
|[AN4397, Common Hardware Design for i.MX 6Dual/6Quad and i.MX 6Solo/6DualLite](https://www.nxp.com/docs/en/application-note/AN4397.pdf) |Jul 2015|
|[i.MX53 DDR Calibration](https://www.nxp.com/docs/en/application-note/AN4466.pdf) |May 2013|
|[i.MX 6 Series DDR Calibration](https://www.nxp.com/docs/en/application-note/AN4467.pdf) |Mar 2015|
|[i.MX 6Dual/6Quad Power Consumption Measurement](https://www.nxp.com/docs/en/application-note/AN4509.pdf) |Oct 2012|
|[i.MX28 Ethernet Performance on Linux](https://www.nxp.com/docs/en/application-note/AN4544.pdf) |Jun 2012|
|[AN4547_wrkng.fm](https://www.nxp.com/docs/en/application-note/AN4547.pdf) |Oct 2012|
|[Using Open Source Debugging Tools for Linux on i.MX Processors](https://www.nxp.com/docs/en/application-note/AN4553.pdf) |Jul 2012|
|[Secure Boot with i.MX28 HAB Version 4](https://www.nxp.com/docs/en/application-note/AN4555.pdf) |May 2013|
|[i.MX 6 DualLite Power Consumption Measurement](https://www.nxp.com/docs/en/application-note/AN4576.pdf) |Mar 2013|
|[i.MX 6 Series Thermal Management Guidelines](https://www.nxp.com/docs/en/application-note/AN4579.pdf) |Dec 2012|
|[AN4580_Rev0_wrkng.fm](https://www.nxp.com/docs/en/application-note/AN4580.pdf) |Dec 2012|
|[AN4589_Rev1.fm](https://www.nxp.com/docs/en/application-note/AN4589.pdf) |Mar 2015|
|[AN4603, Power Management Design Guidelines for the i.MX50x Family of Microprocessors - Application Note](https://www.nxp.com/docs/en/application-note/AN4603.pdf) |Jan 2013|
|[AN4604, Interfacing the MC34709 with the i.MX53 Microprocessor - Application Note](https://www.nxp.com/docs/en/application-note/AN4604.pdf) |May 2013|
|[Interfacing the MC34709 with an External Battery Charger, AN4619 - Application Note](https://www.nxp.com/docs/en/application-note/AN4619.pdf) |Nov 2012|
|[AN4620, Interfacing the MC34708 with an External Battery Charger - Application Note](https://www.nxp.com/docs/en/application-note/AN4620.pdf) |Dec 2012|
|[Fast Image Processing with i.MX 6 Series](https://www.nxp.com/docs/en/application-note/AN4629.pdf) |Mar 2015|
|[AN4641.fm](https://www.nxp.com/docs/en/application-note/AN4641.pdf) |Dec 2012|
|[AN4671, i.MX 6 Series HDMI Test Method for Eye Pattern and Electrical Characteristics - Application Notes](https://www.nxp.com/docs/en/application-note/AN4671.pdf) |Jul 2013|
|[Configuring Secure JTAG for the i.MX 6 Series Family of Applications Processors](https://www.nxp.com/docs/en/application-note/AN4686.pdf) |Apr 2015|
|[AN4715, i.MX 6Solo Power Consumption Measurement - Application Note](https://www.nxp.com/docs/en/application-note/AN4715.pdf) |Oct 2013|
|[IMX6SDL_Product_Lifetime_AN4725.fm](https://www.nxp.com/docs/en/application-note/AN4725.pdf) |Feb 2015|
|[AN4726_IMX6SL_Product_Lifetime.fm](https://www.nxp.com/docs/en/application-note/AN4726.pdf) |Jan 2015|
|[AN4784: PCIe Certification Guide for i.MX 6Dual/6Quad and i.MX 6Solo/6DualLite - Application Note](https://www.nxp.com/docs/en/application-note/AN4784.pdf) |Oct 2013|
|[Doc_Title (same as covered Device #)](https://www.nxp.com/docs/en/application-note/AN4815.pdf) |Feb 2015|
|[i.MX 6 Audio Clock Configuration Options Application Note](https://www.nxp.com/docs/en/application-note/AN4952.pdf) |Jun 2014|
|[i.MX 6SoloX Power Consumption Measurement - AN5050](https://www.nxp.com/docs/en/application-note/AN5050.pdf) |May 2015|
|[AN5072, Introduction to Embedded Graphics with Freescale Devices-Application Notes](https://www.nxp.com/docs/en/application-note/AN5072.pdf) |Feb 2015|
|[AN5078.fm](https://www.nxp.com/docs/en/application-note/AN5078.pdf) |Feb 2015|
|[How to Run the MQX\xe2\x84\xa2 RTOS on Various RAM Memories for i.MX 6SoloX - Application Note](https://www.nxp.com/docs/en/application-note/AN5127.pdf) |May 2015|
|[PCI Express\xc2\xae Certification Guide for the i.MX 6SoloX](https://www.nxp.com/docs/en/application-note/AN5158.pdf) |Jul 2015|
