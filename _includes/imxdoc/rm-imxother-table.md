This report was generated on Thu Aug 09 21:22:50 UTC 2018

|**PDF file** |**Last modification**|
|[IMX50RM.pdf](https://www.nxp.com/docs/en/reference-manual/IMX50RM.pdf) |Fri Jun 19 04:52:08 2015|
|[MCIMX28RM.pdf](https://www.nxp.com/docs/en/reference-manual/MCIMX28RM.pdf) |Mon Aug 19 13:54:15 2013|
|[MCIMX27RM.pdf](https://www.nxp.com/docs/en/reference-manual/MCIMX27RM.pdf) |Wed Jun 20 14:06:59 2012|
|[iMX53RM.pdf](https://www.nxp.com/docs/en/reference-manual/iMX53RM.pdf) |Tue Jun  5 15:39:15 2012|
|[IMX23RM.pdf](https://www.nxp.com/docs/en/reference-manual/IMX23RM.pdf) |Thu Nov 17 09:57:06 2011|
|[IMX35RM.pdf](https://www.nxp.com/docs/en/reference-manual/IMX35RM.pdf) |Fri Nov 12 15:54:01 2010|
|[MCIMX51RM.pdf](https://www.nxp.com/docs/en/reference-manual/MCIMX51RM.pdf) |Thu May 20 11:05:59 2010|
|[MC9328MXSRM.pdf](https://www.nxp.com/docs/en/reference-manual/MC9328MXSRM.pdf) |Wed Jun 13 19:40:21 2007|
|[MCIMX31RM.pdf](https://www.nxp.com/docs/en/reference-manual/MCIMX31RM.pdf) |Fri Dec 19 08:00:54 2008|
|[MC9328MX21RM.pdf](https://www.nxp.com/docs/en/reference-manual/MC9328MX21RM.pdf) |Fri Sep 18 14:16:35 2009|
