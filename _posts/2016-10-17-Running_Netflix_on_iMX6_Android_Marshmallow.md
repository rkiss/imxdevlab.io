---
title: Running Netflix on iMX6 Android Marshmallow
date: 2016-10-17 11:20:06
author: breno
categories: [Tutorial]
tags: [ android, M6.0.1_2.1.0_GA, netflix, IMX6DL, IMX6Q, VPU ]
---

This post explains how to setup Android 6.0.1 Marshmallow and install Netflix
on i.MX6 boards. It covers all the required steps to prepare a android SD card,
install an APK through ADB, add the necessary codecs and configure the system
to reproduce it on 1920x1080p@60.

The video below is an example on a i.MX6DL SabreSD board:

<iframe width="560" height="315" src="https://www.youtube.com/embed/v2OB0ORk5aw" frameborder="0" allowfullscreen></iframe>

## Downloading required software packages:

* Android BSP binary prebuilt image for i.MX6: [Download link][1]
* AAC Plus Codec for i.MX6: [Download link][2]
* Netflix 4.9.4 APK: [Download link][3]
* Simg2img (Converts Android sparse images to raw images): [Download link][4]

if you prefer the simg2img it can be downloaded by using the following command:

```
$ sudo apt-get install zlib1g-dev
```

[1]:https://www.nxp.com/webapp/Download?colCode=IMX6_M6.0.1_2.1.0_AN_DEMO_SD_BSP&appType=license&location=null&fpsp=1&WT_TYPE=Board%20Support%20Packages&WT_VENDOR=FREESCALE&WT_FILE_FORMAT=gz&WT_ASSET=Downloads&fileExt=.gz&Parent_nodeId=1265411638783721675357&Parent_pageType=product
[2]:https://www.nxp.com/webapp/Download?colCode=IMX6-L500-100-AACP-CODEC-CODA&appType=license&location=null&fpsp=1&WT_TYPE=Codecs%20and%20other%20Algorithms&WT_VENDOR=FREESCALE&WT_FILE_FORMAT=gz&WT_ASSET=Downloads&fileExt=.gz&Parent_nodeId=1265411638783721675357&Parent_pageType=product
[3]:http://www.apkmirror.com/apk/netflix-inc/netflix/netflix-4-9-4-build-10056-release/netflix-4-9-4-build-10056-android-apk-download/
[4]:https://github.com/anestisb/android-simg2img

## Setting up ADB:

In order to install an apk without Google Play it’s necessary to install and
setup ADB (Android Debug Bridge)

* Setup ADB udev Rule

```
$ sudo wget -O /etc/udev/rules.d/51-android.rules
https://raw.githubusercontent.com/NicolasBernaerts/ubuntu-
scripts/master/android/51-android.rules
$ sudo chmod a+r /etc/udev/rules.d/51-android.rules
$ sudo service udev restart
```

* Install ADB

```
$ sudo apt-get install android-tools-adb
```
## Preparing Android SD card:

* Format a SD card and create following store partitions:

<img src="{{ site.url }}{{ site.baseurl }}/images/android_sd_card.jpg">

## Adding AAC Plus Codec in the prebuilt image:

* Unpack the downloaded packets and modify the system_raw.img:

```
$ tar xzvf android_M6.0.1_2.1.0_image_6dqpsabresd.tar.gz &
fsl_aacp_dec_L5.0.0_1.0.0-ga.tar.gz
$ cd android_M6.0.1_2.1.0_image_6dqpsabresd/SD
$ simg2img system.img system_raw.img
$ sudo mount -t ext4 -o loop system_raw.img /mnt
$ cd /PATH/TO/YOUR/CODEC/fsl_aacp_dec
$ sudo cp *.so /mnt/lib
$ sudo umount /mnt
```

# Flashing the images:

* Once your system_raw image contains the necessary codecs the images can
be flashed on the SD card:

```
$ cd android_M6.0.1_2.1.0_image_6dqpsabresd/SD
$ sudo dd if=u-boot-imx6q.imx of=/dev/sdX bs=1K seek=1; sync
$ cd SD
$ sudo dd if=boot-imx6q.img of=/dev/sdX1; sync
$ sudo dd if=system_raw.img of=/dev/sdX5; sync
$ sudo dd if=recovery-imx6q.img of=/dev/sdX2; sync
```
## Configuring the first boot (LVDS):

We are going to use lvds just for setup and install the Netflix APK, it’s not
possible to connect a mouse while using adb.

* Plug your SD on SD3, set your SW6 to 01000010 and stop on u-boot:

```
$ setenv bootargs
$ setenv fastboot_dev mmc1
$ setenv bootcmd boota mmc1
$ setenv bootargs console=ttymxc0,115200 init=/init video=mxcfb0:dev=ldb,bpp=32 video=mxcfb1:off video=mxcfb2:off video=mxcfb3:off vmalloc=256M androidboot.console=ttymxc0 androidboot.dm_verity=disabled consoleblank=0 androidboot.hardware=freescale cma=384M
$ saveenv
```
## Installing APK with ADB:

* In order to install APK thorough ADB is necessary to enable the developer
mode:
* On your device Go to settings → About tablet
* Click in build number 7 times
* Once this step is done, go to settings → developer options and enable the
USB Debugging and stay awake options.
* On your PC go to the apk directory and install it through ADB:

```
$ sudo adb install netflix4.9.4.apk
```

## Running netflix (HDMI):

* Reboot your device and stop on u-boot
* Set your bootargs in order to use HDMI

```
$ setenv bootargs console=ttymxc0,115200 init=/init video=mxcfb1:off
video=mxcfb0:dev=hdmi,1920x1080M@60,if=RGB24,bpp=32
video=mxcfb2:off video=mxcfb3:off vmalloc=256M
androidboot.console=ttymxc0 androidboot.dm_verity=disabled
consoleblank=0 androidboot.selinux=disabled
androidboot.hardware=freescale cma=384M
$ saveenv
```

* Reboot your device.
* Check your Ethernet connection and setup the date and time.
* Run Netflix.

## Testing system performance:

In order to check the board performance the following commands can be used:

* CPU usage:

```
$ top | grep netflix
1339  2  10% S    66 1697016K 141236K  fg u0_a49   com.netflix.mediaclient
```

* RAM usage:

```
$ vmstat
procs -----------memory---------- ---swap-- -----io---- -system-- ----cpu----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa
 2  0      0 246876   3208 300180    0    0    95     4    0  238  1  1 97  0
```

* VPU interrupts:

```
$ cat proc/interrupts | grep VPU
28:          0          0          0          0       GPC   3 Edge      VPU_JPG_IRQ
29:       7075          0          0          0       GPC  12 Level     VPU_CODEC_IRQ
```
