---
title: Gateworks plugin RTSP variable is supported on Yocto Community now
date: 2017-08-25 13:56:46
author: marco
categories: [ Tutorial ]
tags: [ fsl-community-bsp, VPU, GStreamer, rtsp, H264, encoder, decoder, IMX6Q, IMX6DL ]
---
Recently was shared a [post](https://imxdev.gitlab.io/tutorial/i.MX_6Q_RTSP_video_and_camera_stream) explaining how to use **gstreamer1.0-rtsp-server** package on i.MX6DL/Q SABRE-SD to stream video files and camera using RTSP protocol.

However that post shares an approach not direct.

One solution for this is use the [Gateworks plugin](https://github.com/Gateworks/gst-gateworks-apps) **gst-variable-rtsp-server**, which offers a direct way for **gstreamer1.0-rtsp-server**, with the ability to dynamically change the bitrate of the video stream on the fly.

Now, thanks to [Vanessa](https://www.linkedin.com/in/vanessa-maegima), the **gst-variable-rtsp-server** is supported in the [FSL Community BSP](https://github.com/Freescale/fsl-community-bsp-platform) and here you can found how to install and use it:

## How to install

1. Prepare your machine and distro, as the example below:
```
MACHINE=imx6qdlsabresd DISTRO=fslc-x11 source setup-environment build
```
2. Add the following on your `build/conf/local.conf`:  
```
CORE_IMAGE_EXTRA_INSTALL += "gst-variable-rtsp-server"
```
3. Run ```bitbake fsl-image-multimedia-full``` and mount your ```sdcard```.

You can use another distro or image, but be sure to include all you need to support your GStreamer pipelines.
{: .notice--info}

## GStreamer pipelines

Check the section ```preparing the boards``` in the [RTSP video and camera stream post](https://imxdev.gitlab.io/tutorial/i.MX_6Q_RTSP_video_and_camera_stream) to be able to test the GStreamer pipelines below.

### Video test source example
* SERVER:
```
=> gst-variable-rtsp-server -p 9001 -u "videotestsrc ! imxvpuenc_h264 ! rtph264pay name=pay0 pt=96"
```
* CLIENT:
```
gst-launch-1.0 rtspsrc location=rtsp://$SERVERIP:9001/stream ! queue ! decodebin ! autovideosink
```

### Camera example
* SERVER:
```
=> gst-variable-rtsp-server -p 9001 -u "imxv4l2videosrc device=/dev/video0 ! capsfilter caps='video/x-raw, width=1280, height=720, framerate=30/1' ! imxvpuenc_h264 ! rtph264pay name=pay0 pt=96"
```
* CLIENT:
```
gst-launch-1.0 rtspsrc location=rtsp://$SERVERIP:9001/stream latency=100 buffer-mode=slave ! decodebin ! autovideosink sync=false
```
