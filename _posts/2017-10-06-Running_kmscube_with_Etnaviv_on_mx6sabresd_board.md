---
title: Running kmscube with Etnaviv on mx6sabresd board
date: 2017-10-06 15:22:45
author: fabio
categories: [Tutorial]
tags: [IMX6, etnaviv, kmscube, buildroot]
---
The Etnaviv developers have made outstanding progress making it possible
to run 3D graphics applications on i.MX6 with mainline kernel and standard
open source userspace components such as mesa and libdrm.

There is a [mailing list](https://lists.freedesktop.org/mailman/listinfo/etnaviv)
to discuss Etnaviv related issues.

How can we easily test the Etnaviv project?
-------------------------------------------

Etnaviv can run on your favorite build system/Linux distro. As I have been using
it with Buildroot, this post shows how to build a Buildroot image with
Etnaviv support on i.MX6.

In general the Buildroot configs tend to generate a minimal rootfs, but in some
cases a more complete defconfig can be accepted. I have added support for
the `imx6-sabresd_qt5_defconfig` target in Buildroot that includes mainline
U-Boot, mainline kernel, mesa, libdrm, kmscube, Qt5, glmark2 application.

The `imx6-sabresd_qt5_defconfig` can be found [here](https://git.buildroot.net/buildroot/tree/configs/imx6-sabresd_qt5_defconfig)

With the `imx6-sabresd_qt5_defconfig` target we can easily see Etnaviv in action.

A good way to test Etnaviv is by running a popular OpenGLEs application called
kmscube. This project is hosted at: (https://cgit.freedesktop.org/mesa/kmscube/)

In order to generate a Buildroot image with Etnaviv support for mx6sabresd:

```console
$ make imx6-sabresd_qt5_defconfig
$ make
```

This will download and build all the software components from scratch, so it
may take a while.

After the build process is finished an sdcard.img file will be available
at `output/images/sdcard.img`.

Just flash it directly to the SD card:

```console
$ sudo dd if=output/images/sdcard.img of=/dev/mmcblk0; sync
```

Then boot the mx6sabresd board (Since this image uses SPL, it can run on any
mx6sabresd board variant: mx6q, mx6dl and mx6qp).

The default password is `root`.

We are ready now to run the kmscube application by simply doing:

```console
# kmscube
```

And a nice colored cube will spin on the screen.

<iframe width="560" height="315"
  src="https://www.youtube.com/embed/iTWeRzOkzw4" frameborder="0" allowfullscreen>
</iframe>

The application can be stopped by doing CTRL+C. Re-launch it as

```console
# kmscube &
```

and the system interrupts can be observed by doing:

```console
# cat /proc/interrupts  | grep gpu
 20:       2436          0          0          0       GPC   9 Level     130000.gpu
 21:          0          0          0          0       GPC  10 Level     134000.gpu
 75:          0          0          0          0       GPC  11 Level     2204000.gpu
```

Running the `cat` command several times shows that the hardware GPU
interrupt count keep increasing, indicating that the application is being
accelerated by the GPU hardware on i.MX6.

kmscube also allows to render video on top of the cube faces. I haven't tried
this yet, but will do it when time permits, as it is a nice example of
integrating both the GPU and VPU capabilities of the i.MX6.

Feel free to ping us if you manage to get the kmscube video demo working on
i.MX6.
