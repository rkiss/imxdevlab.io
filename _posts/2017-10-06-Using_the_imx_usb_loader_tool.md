---
title: Using the imx_usb_loader tool
date: 2017-10-06 09:33:19
author: fabio
categories: [Tutorial]
tags: [imx_usb_loader, IMX5, IMX6, IMX7, VYBRID, SDP, SPL, uboot]
---

The [`imx_usb_loader`](https://github.com/boundarydevices/imx_usb_loader) tool
is a very helpful utility that allows to download and execute code on 
NXP i.MX5/i.MX6/i.MX7 and Vybrid SoCs through the Serial Download Protocol
(SDP).

It really helps the development process as it provides a simple way to load
U-Boot directly to DRAM.

The imx_usb_loader tool can be retrieved from the Boundary Devices git
repository:

```console
$ git clone https://github.com/boundarydevices/imx_usb_loader.git
$ cd imx_usb_loader
```

To build the tool the libusb-1.0 package needs to be installed in the host PC,
then simply do:

```console
$ make
```

Then `imx_usb` binary is generated.

Using imx_usb_loader tool to load U-Boot on a imx7d-sabresd board
-----------------------------------------------------------------

Here are the steps for loading U-Boot on a imx7d-sabresd board via
imx_usb_loader tool:

1. Build U-Boot for mx7d-sabresd:

```console
$ make mx7dsabresd_defconfig
$ make -j4
```

In U-Boot 2017.09 the resultant binary for this board is called `u-boot-dtb.imx`

The `-dtb` prefix indicates that this board uses device tree to represent the
hardware description to U-Boot.

2. Copy `u-boot-dtb.imx` into the imx_usb_loader tool directory.

3. Put the imx7d-sabresd board in Serial Download mode, which can be done by
removing the SD card from the `SD1` boot slot. In this case, when i.mx7 boots
the ROM code will not find a valid boot medium and then will enter into serial
download mode.

- Connect a USB cable to the debug UART for console and one USB cable between
the host PC and the imx7d-sabresd USB OTG port.

- Power on the board. As i.MX7 boots in serial download mode,
the following messages can be observed by running `dmesg` on the host PC:

```
[89233.440935] usb 2-4: new high-speed USB device number 29 using xhci_hcd
[89233.569562] usb 2-4: New USB device found, idVendor=15a2, idProduct=0076
[89233.569566] usb 2-4: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[89233.569568] usb 2-4: Product: SE Blank ULT1
[89233.569570] usb 2-4: Manufacturer: Freescale SemiConductor Inc 
[89233.570741] hid-generic 0003:15A2:0076.0005: hiddev0,hidraw0: USB HID v1.10 Device [Freescale SemiConductor Inc  SE Blank ULT1] on usb-0000:00:14.0-4/input0

```
From the `imx_usb_loader` directory run:

```console
$ sudo ./imx_usb u-boot-dtb.imx
```

and then U-Boot boot message can be seen in the serial console:

```
U-Boot 2017.09 (Oct 05 2017 - 10:09:20 -0300)

CPU:   Freescale i.MX7D rev1.2 1000 MHz (running at 792 MHz)
CPU:   Commercial temperature grade (0C to 95C) at 40C
Reset cause: POR
Model: Freescale i.MX7 SabreSD Board
Board: i.MX7D SABRESD in non-secure mode
DRAM:  1 GiB
PMIC: PFUZE3000 DEV_ID=0x30 REV_ID=0x11
MMC:
MMC Device 0 not found
*** Warning - No MMC card found, using default environment

Video: 480x272x24
In:    serial
Out:   serial
Err:   serial
Net:   FEC0
Hit any key to stop autoboot:  0
```

The `MMC Device 0 not found` message is expected because in order to force
entering in serial download mode, I simply removed the SD card from the slot.

In case the `imx_usb` tool is run with no root permission, the following error
message is seen:

```
$ ./imx_usb u-boot-dtb.imx
config file <.//imx_usb.conf>
vid=0x066f pid=0x3780 file_name=mx23_usb_work.conf
vid=0x15a2 pid=0x004f file_name=mx28_usb_work.conf
vid=0x15a2 pid=0x0052 file_name=mx50_usb_work.conf
vid=0x15a2 pid=0x0054 file_name=mx6_usb_work.conf
vid=0x15a2 pid=0x0061 file_name=mx6_usb_work.conf
vid=0x15a2 pid=0x0063 file_name=mx6_usb_work.conf
vid=0x15a2 pid=0x0071 file_name=mx6_usb_work.conf
vid=0x15a2 pid=0x007d file_name=mx6_usb_work.conf
vid=0x15a2 pid=0x0080 file_name=mx6_usb_work.conf
vid=0x15a2 pid=0x0076 file_name=mx7_usb_work.conf
vid=0x10c4 pid=0xea70 file_name=mx7ulp_usb_work.conf
vid=0x1fc9 pid=0x0126 file_name=mx7ulp_arm2_usb_work.conf
vid=0x15a2 pid=0x0041 file_name=mx51_usb_work.conf
vid=0x15a2 pid=0x004e file_name=mx53_usb_work.conf
vid=0x15a2 pid=0x006a file_name=vybrid_usb_work.conf
vid=0x066f pid=0x37ff file_name=linux_gadget.conf
Could not open device vid=0x15a2 pid=0x80: Access denied (insufficient permissions), err=-3
```

The method above works well for loading a single U-Boot binary like
`u-boot-dtb.imx` and `u-boot.imx`, but what if we have a U-Boot system that uses
SPL mechanism instead?

Using imx_usb_loader tool to load U-Boot on a imx6-sabresd board
-----------------------------------------------------------------

Thanks to the great work from Stefan Agner at Toradex it is now possible
to use imx_usb_loader to also load SPL and `u-boot.img` binaries.

Details can be found on the `doc/README.sdp` file inside the U-Boot tree.

Here is the [patch](http://git.denx.de/?p=u-boot.git;a=commitdiff;h=031426a7af63d3c939fc963311e6dc8e904a0440;hp=511db3bf5a27a2f7eca0f60b6adb1eff74bd6df6)
I did for adding Serial Download Protocal support for the mx6sabresd board which
will be part of U-Boot 2017.11:

1. Build U-Boot for mx6sabresd:

```console
$ make mx6sabresd_defconfig
$ make -j4
```

2. Copy SPL and `u-boot.img` binaries into the imx_usb_loader tool directory.

```console
$ sudo ./imx_usb SPL
```

Then the following message is seen on the serial console:

```
U-Boot SPL 2017.11-rc1-00025-g3e0c8f7 (Oct 05 2017 - 13:48:46)
Trying to boot from USB SDP
SDP: initialize...
SDP: handle requests...
```

Now it is time to send the U-Boot proper (`u-boot.img`) binary:

```console
$ sudo ./imx_usb u-boot.img
```

And the serial console will show:

```
Downloading file of size 389656 to 0x177fffc0... done
Jumping to header at 0x177fffc0
Header Tag is not an IMX image

U-Boot 2017.11-rc1-00025-g3e0c8f7 (Oct 05 2017 - 13:48:46 -0300)

CPU:   Freescale i.MX6Q rev1.2 996 MHz (running at 792 MHz)
CPU:   Automotive temperature grade (-40C to 125C) at 44C
Reset cause: POR
Board: MX6-SabreSD
I2C:   ready
DRAM:  1 GiB
PMIC:  PFUZE100 ID=0x10
MMC:   FSL_SDHC: 0, FSL_SDHC: 1, FSL_SDHC: 2
MMC: no card present
mmc_init: -123, time 2
*** Warning - MMC init failed, using default environment

PCI:   pcie phy link never came up
No panel detected: default to Hannstar-XGA
Display: Hannstar-XGA (1024x768)
In:    serial
Out:   serial
Err:   serial
Net:   FEC [PRIME]
Hit any key to stop autoboot:  0 
=> 
```

As it is explained in the `doc/README.sdp` file it is possible to change the
`mx6_usb_rom.conf` and `mx6_usb_sdp_spl.conf` files  so that a single invocation
of `imx_usb` could load and run both `SPL` and `u-boot.img` binaries.

This tool is also flexible in allowing cascading more binaries, like `zImage`,
`dtb` and a `ramdisk`, which means that it is possible to boot a complete
Linux image via serial download protocol.
