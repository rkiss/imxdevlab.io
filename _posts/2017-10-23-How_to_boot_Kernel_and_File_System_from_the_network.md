---
title: How to boot Kernel and access root file system from the network
date: 2017-10-23 09:53:11
author: breno
categories: [ Tutorial ]
tags: [ TFTP, NFS, tftpboot, kernel, rootfs, network ]
---

This tutorial explains how to load a Kernel image using TFTP (Trivial File
Transfer Protocol) and access a root file system located your host PC through
the NFS (Network File System) protocol. This mechanism can be handy during the
software development process, not being necessary to flash the SD card for
every test.

## Configuring your Host PC for TFTP

The first step is to install all the prerequisite packages for TFTP:

```console
$ sudo apt-get install xinetd tftpd tftp
```

Create a TFTP folder in your desired location with root owner and the "rwx"
permission for all users:

```console
$ sudo mkdir /tftpboot
$ sudo chmod –R 777 /tftpboot
$ sudo chown –R root /tftpboot
```

Create a configuration file for the TFTP with the following content. (The
`server_args` parameter must match with the folder created above)

```console
$ cat /etc/xinetd.d/tftp

service tftp
{
	protocol	= udp
	port		= 69
	socket_type	= dgram
	wait		= yes
	user		= root
	server		= /usr/sbin/in.tftpd
	server_args	= -s /tftpboot
	disable		= no
}
```

Restart the `xinetd` service:

```console
$ sudo /etc/init.d/xinetd restart
```

You can place any file at the TFTP folder and load it through U-Boot, you can
also create symbolic links from your building directory avoiding to copy and
paste your `zImage` and `dtb` files every time.

## Configuring your Host PC for NFS

Install all the needed packages for NFS:

```console
$ sudo apt-get install nfs-kernel-server
```

Create a folder for placing your rootfs:
```console
$ mkdir /tftpboot/rfs
```

Add the following line in the end of your `/etc/exports` file:
```
/tftpboot/rfs *(rw,no_root_squash,no_subtree_check)
```
Restart the NFS service:
```console
$ sudo service nfs-kernel-server restart
```

Place your rootfs or create a symbolic link for the NFS folder.

## Configuring your board

Stop in U-Boot prompt and set the following environments variables:

```
=> setenv serverip <YOUR_TFTP_HOST_IP>
=> setenv ip_dyn yes
=> setenv nfsroot /tftpboot/rfs
=> setenv bootcmd "run netboot"
=> saveenv
=> reset
```

If everything is correct your next boot will be using the binaries from the
network.

In some boards the Ethernet `MAC Address` is not written into the eFuses and
may be necessary to set a random address manually:
```
=> setenv ethaddr 00:05:9F:04:36:2A
```

## Transferring a file through TFTP

U-Boot also includes the `tftp` tool that loads a file from your host PC to a
desired memory location in your board using the TFTP. This can be used for
flashing your MMC or SPI memory using the U-Boot terminal.

In U-Boot prompt set the following environment variables:

```
=> setenv serverip <YOUR_TFTP_HOST_IP>
=> setenv ipaddr  <YOUR_BOARD_IP>
```
After configuring your variables you can load a file using `tftp` command,
for example loading a binary containing 1Kb of zeros at the address 0x8080000:

Current content at 0x80800000:
```
=> md 0x80800000
80800000: d128ba73 28c5202a 4cc73d74 e2869d87    s.(.* .(t=.L....
80800010: 8800ae80 f810694c c2816011 283f25d0    ....Li...`...%?(
80800020: 1cf22d13 0b250ca5 752e0f92 c7598824    .-....%....u$.Y.
80800030: 311921c0 061ad180 64112e01 491da883    .!.1.......d...I
80800040: 1da3b652 8c8a38c8 641a5e8c 9112620a    R....8...^.d.b..
80800050: a710a03b 2a9a4fba a0fdecca 968fdb24    ;....O.*....$...
80800060: 21651362 30002820 a30f5318 d382d030    b.e! (.0.S..0...
80800070: 903f5fa7 598658a9 f6c28d03 f7e88091    ._?..X.Y........
80800080: 699057d0 03d310da 57140441 77d75c24    .W.i....A..W$\.w
80800090: 3964a44c 2e462224 c25c350e 0e363354    L.d9$"F..5\.T36.
808000a0: a6a94c13 ddaf07e2 b00ce470 aa34d050    .L......p...P.4.
808000b0: 250478e8 9c754a33 087141b3 b7874830    .x.%3Ju..Aq.0H..
808000c0: 5867c850 1670bce5 fd5110a2 2fc08c00    P.gX..p...Q..../
808000d0: 70822106 e9c1d226 27798c0b 6a309e48    .!.p&.....y'H.0j
808000e0: 6540773c f4b0771d 8b2550a5 5bb652fd    <w@e.w...P%..R.[
808000f0: 79088f18 39408923 278208eb 2403e73e    ...y#.@9...'>..$
```

Transferring the file to 0x80800000:
```
=> tftp 0x80800000 zero.bin
Using FEC1 device
TFTP from server x.x.x.x; our IP address is x.x.x.x; sending through gateway x.x.x.x
4
Filename 'zero.bin'.
Load address: 0x80800000
Loading: #
         166 KiB/s
done
Bytes transferred = 1024 (400 hex)
```

New content at 0x80800000:
```
=> md 0x80800000
80800000: 00000000 00000000 00000000 00000000    ................
80800010: 00000000 00000000 00000000 00000000    ................
80800020: 00000000 00000000 00000000 00000000    ................
80800030: 00000000 00000000 00000000 00000000    ................
80800040: 00000000 00000000 00000000 00000000    ................
80800050: 00000000 00000000 00000000 00000000    ................
80800060: 00000000 00000000 00000000 00000000    ................
80800070: 00000000 00000000 00000000 00000000    ................
80800080: 00000000 00000000 00000000 00000000    ................
80800090: 00000000 00000000 00000000 00000000    ................
808000a0: 00000000 00000000 00000000 00000000    ................
808000b0: 00000000 00000000 00000000 00000000    ................
808000c0: 00000000 00000000 00000000 00000000    ................
808000d0: 00000000 00000000 00000000 00000000    ................
808000e0: 00000000 00000000 00000000 00000000    ................
808000f0: 00000000 00000000 00000000 00000000    ................
```
