---
title: How to build and flash a bootable image for Cortex-M4 on i.MX 7ULP
date: 2018-06-04 13:10:01
author: vanessa
categories: [ tutorial ]
tags: [ m4, freertos, sdk, mcuxpresso, IMX7ULP, qspi ]
---

The i.MX 7ULP processor aims on reduced power consumption (**U**ltra **L**ow
**P**ower) and for this reason its architecture is a bit different than the other
i.MX processors. To guarantee less power consumption, the Cortex-M4 works as the
primary core and is responsible for controlling the Cortex-A7 power rails, being
able to suspend A7 in run time. With this in mind, to boot an image on A7 all
needed power rails must be enabled on the M4 side.

This tutorial shows how to build and flash a demo image for the M4 core in the
QSPI flash with all needed configuration that allows booting along with the A7
core.

{: .notice--info}
**NOTE**: The `REL_SDK_ULP1_B0_2.4.0_RFP` package provides a prebuilt image ready
to be flashed to the chip (`power_mode_switch.img`). If the reader chooses to use
this image, copy `power_mode_switch.img` to the boot partition of the SD card and
jump to the last section (***Flashing the image***).

# Setting up the machine

{: .notice--info}
**NOTE**: This shows the procedure on a Linux environment. For Windows OS, please
see the **Getting Started** documentation on the SDK package.

- Install `cmake` on the host machine:

```
$ sudo apt-get install cmake
```

- Download the `armgcc` toolchain from
[here](https://launchpad.net/gcc-arm-embedded/5.0/5-2016-q2-update/+download/gcc-arm-none-eabi-5_4-2016q2-20160622-linux.tar.bz2)
and export the location as `ARMGCC_DIR`:

```
$ export ARMGCC_DIR=<your_path_to_arm_gcc>/gcc-arm-none-eabi-5_4-2016q2/
```

{: .notice--info}
**NOTE**: The `ARMGCC_DIR` variable needs to be exported on the terminal used
for compilation.

# Downloading the SDK

Download the [MCUXpresso SDK](https://mcuxpresso.nxp.com/en/welcome) following
these steps:

- Click on "*Select Development Board*";

- Select `EVK-MCIMX7ULP` under "*Select a Device, Board, or Kit*" and click on
"*Build MCUXpresso SDK*" on the right;

<img src="{{ base_path }}/images/sdk_build.png">

- Select "*Host OS*" as `Linux` and "*Toolchain/IDE*" as `GCC ARM Embedded`;

- Add "FreeRTOS" and all the wanted Middleware and hit "*Request Build*";

- Wait for the SDK to build and download the package.

# Building the image

All demos and code examples available on the SDK package are located in the
directory `<<SDK_dir>>/boards/evkmcimx7ulp/`. This tutorial shows how to build
and flash the `power_mode_switch` demo which includes all needed configuration on
M4 to boot the A7 core but the same procedure works for any example on the SDK.

- To build the demo, enter the *armgcc* folder under the demo directory and make
sure that the `ARMGCC_DIR` variable is set correctly.

```
$ cd <SDK_dir>/boards/evkmcimx7ulp/demo_apps/power_mode_switch/armgcc
$ export ARMGCC_DIR=<your_path_to_arm_gcc>/gcc-arm-none-eabi-5_4-2016q2/
```

- Run the `build_release.sh` script to build the code.

```
$ ./build_release.sh
```

This generates the M4 binary (`sdk20-app.bin`) under the *release* folder. To
make this binary bootable, the `imgutil` utility is used to add all needed header
to the image.

- Copy `sdk20-app.bin` to the *evkmcimx7ulp* folder under the *imgutil* tool:

```
$ cp sdk20-app.bin <SDK_dir>/tools/imgutil/evkmcimx7ulp
```

- Run the `mkimg.sh` script to generate the bootable image appending the linker
type used to build the binary. The linker points to the memory region where the
M4 image runs and it can be whether `ram` or `flash` depending on what is
defined on the `CMakeLists.txt` file. In this case the image was built for `ram`:

```
$ ./mkimg.sh ram
```

This generates the bootable image `sdk20-app.img`. Copy this image to the boot
partition of the SD Card.

# Flashing the image in the QSPI flash

- Open two serial consoles, one at `/dev/ttyUSB0` for A7 to boot Linux and other
at `/dev/ttyUSB1` for M4 to boot the SDK image.

- On the A7 console, stop U-Boot and enter the following commands to load the
M4 binary to RAM and flash it in the QSPI memory:

```
=> sf probe
=> sf erase 0x0 0x20000
=> fatload mmc 0:1 0x62000000 sdk20-app.img
=> sf write 0x62000000 0x0 0x20000
```

{: .notice--info}
**NOTE**: If the reader is using the prebuilt image (`power_mode_switch.img`),
replace the *fatload* command above accordingly.

- Reset the board to boot both cores. The M4 core loads the image from QSPI
memory with the needed configuration to boot A7 and A7 fully boots Linux.
