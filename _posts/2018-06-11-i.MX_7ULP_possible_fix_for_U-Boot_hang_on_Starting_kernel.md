---
title: i.MX 7ULP possible fix for U-Boot hang on "Starting kernel"
date: 2018-06-11 12:23:24
author: vanessa
categories: [ tutorial ]
tags: [ IMX7ULP, kernel, error, boot, m4 ]
---

Sometimes when working with i.MX 7ULP the Cortex-A7 loading process can get stuck
in the "Starting kernel" message. This can happen for many reasons, but a very
common cause for this hang is the lack of the Cortex-M4 image that enables all
needed power rails for Cortex-A7 to boot correctly on i.MX 7ULP.

<img src="{{ base_path }}/images/ulp_hang.png">

The picture above shows the A7 booting process stuck when there is no content on
QSPI flash.

This post shows how to flash the bootable M4 image to QSPI and enable booting the
A7 core.

# Getting the M4 bootable image

The SDK package for the i.MX 7ULP Cortex-M4 can be found at
[MCUXpresso](https://mcuxpresso.nxp.com/en/welcome).

- Copy the bootable M4 image `power_mode_switch.img` to the boot partition of the
SD card.

```
$ cp <SDK_dir>/boards/evkmcimx7ulp/demo_apps/power_mode_switch/power_mode_switch.img \
<path_to_boot_partition>
```

{: .notice--info}
To learn how to download and build the SDK package for M4, please see
[this](https://imxdev.gitlab.io/tutorial/How_to_build_and_flash_a_bootable_image_for_Cortex-M4_on_i.MX_7ULP/) post.

# Flashing the M4 image to QSPI

- Open two serial consoles, one at `/dev/ttyUSB0` for A7 to boot Linux and other
at `/dev/ttyUSB1` for M4 to boot the SDK image.

- On the A7 console, stop U-Boot and enter the following commands to load the
M4 binary to RAM and flash it in the QSPI memory:

```
=> sf probe
=> sf erase 0x0 0x20000
=> fatload mmc 0:1 0x62000000 sdk20-app.img
=> sf write 0x62000000 0x0 0x20000
```

After flashing the M4 image to QSPI, reboot the board to boot both cores. This
fixes the loading hang on Cortex-A7.

This procedure works for booting on 'Dual Boot' mode (Cortex-M4 as the primary
core and Cortex-A7 as the auxiliary core). For details about different boot modes
and how to boot in each one of them check [this](https://imxdev.gitlab.io/tutorial/i.MX_7ULP_Heterogeneous_Architecture_and_Different_Boot_Modes/)
post.
