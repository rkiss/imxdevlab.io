---
title: A collection of helpful documentation links
date: 2018-07-06 12:08:01
author: daiane
categories: [Blog]
---
{% include base_path %}

The i.MX Dev Blog includes a collection of links for the most useful documents
usually needed when working with i.MX.

There is a link in the blog upper bar (called **i.MX DOCs**) pointing to the
collection. The first page you see is the table with links to the i.MX6
Reference Manuals.

Then you can choose to see other i.MX Reference Manuals, or Chip Errata.

We gathered all the Application Notes published and listed then in a table with
the last modification date. So far we use only the `pdf` metadata to create the
table, but in future we might be able to add more code on that.

* Software Docs     - [{{ base_path }}/imxdoc/software-links/]({{ base_path }}/imxdoc/software-links/)
* Application Notes - [{{ base_path }}/imxdoc/application-notes/]({{ base_path }}/imxdoc/application-notes/)
* Reference Manual 
  * iMX8            - [{{ base_path }}/imxdoc/reference-manual-imx8/]({{ base_path }}/imxdoc/reference-manual-imx8/)
  * iMX7            - [{{ base_path }}/imxdoc/reference-manual-imx7/]({{ base_path }}/imxdoc/reference-manual-imx7/)
  * iMX6            - [{{ base_path }}/imxdoc/reference-manual-imx6/]({{ base_path }}/imxdoc/reference-manual-imx6/)
  * Mature SoCs     - [{{ base_path }}/imxdoc/reference-manual-other/]({{ base_path }}/imxdoc/reference-manual-other/)
* Chip Errata
  * iMX8            - [{{ base_path }}/imxdoc/chip-errata-imx8/]({{ base_path }}/imxdoc/chip-errata-imx8/)
  * iMX7            - [{{ base_path }}/imxdoc/chip-errata-imx7/]({{ base_path }}/imxdoc/chip-errata-imx7/)
  * iMX6            - [{{ base_path }}/imxdoc/chip-errata-imx6/]({{ base_path }}/imxdoc/chip-errata-imx6/)
  * Mature SoCs     - [{{ base_path }}/imxdoc/chip-errata-other/]({{ base_path }}/imxdoc/chip-errata-other/)