#/bin/sh

date=`date +%Y-%m-%d`
datetime=`date "+%Y-%m-%d %H:%M:%S"`

if [ -z "$1" ]
  then
    echo "Change the date and time of post and file name
It uses the system date and time.
   Usage:
     $0 <path to the post file to be updated>

"
  exit
fi

new_file=$(echo $1 | sed -r "s/[12][0-9]{3}-[01][0-9]-[0-3][0-9]/$date/g")

sed -i "/date/ s/: .*/: $datetime/" $1

mv $1 $new_file

echo "Please see the change with
git status
old file: $1
new file: $new_file"
